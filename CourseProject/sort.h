#ifndef SORT_H_
#define SORT_H_

#include <queue>
#include <stack>

#define ASC	 0
#define DESC 1

int SortIndex(std::queue<int> &q, int sortedIndex, bool order);
void InsertToRear(std::queue<int> &q, int minIndex);
void SortQueue(std::queue<int> &q, bool order);
void QueuesToStack(std::queue<int> &firstQ, std::queue<int> &secondQ, std::stack<int> &stack);
void SortBothQueues(std::queue<int> &firstQ, std::queue<int> &secondQ, bool order);

#endif /* SORT_H_ */