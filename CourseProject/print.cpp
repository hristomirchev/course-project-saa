#include <iostream>
#include <fstream>
#include "print.h"

void PirntQueue(std::queue<int> &q)
{
	std::queue<int> tempQ = q;

	while (tempQ.empty() == false)
	{
		std::cout << tempQ.front() << std::endl;
		tempQ.pop();
	}
}

void PrintBothQueues(std::queue<int> &firstQ, std::queue<int> &secondQ)
{
	std::queue<int> tempQ1 = firstQ;
	std::queue<int> tempQ2 = secondQ;

	//for (int i = 0, j = 0; i < sizeOfFirst, j < sizeOfSecond; i++, j++)
	while(tempQ1.empty() == false && tempQ2.empty() == false)
	{
		std::cout << tempQ1.front() << "\t";
		tempQ1.pop();
		std::cout << tempQ2.front() << std::endl;
		tempQ2.pop();
	}
}

void PrintSizeOfQueues(std::queue<int> &firstQ, std::queue<int> &secondQ)
{
	std::cout << "Size of first queue: " << firstQ.size();
	std::cout << "Size of second queue: " << secondQ.size();
}

void PrintStackFromFile(const char *fileName)
{
	std::ifstream file(fileName);

	int a;

	while (file >> a)
	{
		std::cout << a << std::endl;
	}

	file.close();
}

void PrintNumbersFromFile(const char* fileName)
{
	std::ifstream readFile(fileName);

	int a, b;

	while (readFile >> a >> b)
	{
		std::cout << a << "\t" << b << std::endl;
	}

	readFile.close();
}

void PrintStack(std::stack<int> &stack)
{
	std::stack<int> tempStack = stack;

	while (!tempStack.empty())
	{
		std::cout << tempStack.top() << std::endl;
		tempStack.pop();
	}
}