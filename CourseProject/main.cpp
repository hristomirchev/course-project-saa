#include <queue>
#include <iostream>
#include <fstream>
#include <ctime>
#include "sort.h"
#include "files_functionality.h"
#include "defs.h"
#include "print.h"
#include "menu.h"

int main()
{
	// Declare two queues
	std::queue<int> firstQueue;
	std::queue<int> secondQueue;
	std::stack<int> stack;

	//GenerateRandomNumbersInFile(DATA_FILE_NAME);
	//std::cout << "\nThese are the random generated numbers read from the file.\n\n";
	//PrintNumbersFromFile(DATA_FILE_NAME);
	//FillQueuesFromFile(firstQueue, secondQueue, DATA_FILE_NAME);

	//SortBothQueues(firstQueue, secondQueue,ASC);

	//PrintQueue(firstQueue);
	//PrintBothQueues(firstQueue, secondQueue);

	//QueuesToStack(firstQueue, secondQueue, stack);
	//PrintStack(stack);
	//OutputStackToFile(stack, OUTPUT_FILE_NAME);

	do
	{
		switch (menu())
		{
		case 1:
			GenerateRandomNumbersInFile(QUEUES_FILE_NAME);
			FillQueuesFromFile(firstQueue, secondQueue, QUEUES_FILE_NAME);
			break;
		case 2:
			PrintNumbersFromFile(QUEUES_FILE_NAME);
			break;
		case 3:
			SortBothQueues(firstQueue, secondQueue, ASC);
			break;
		case 4:
			PrintBothQueues(firstQueue, secondQueue);
			break;
		case 5:
			QueuesToStack(firstQueue, secondQueue, stack);
			break;
		case 6:
			PrintStack(stack);
			break;
		case 7:
			OutputStackToFile(stack, STACK_FILE_NAME);
			break;
		case 8:
			PrintStackFromFile(STACK_FILE_NAME);
			break;
		case 0:
			exit(0);
			break;
		default:
			std::cout << "\nEnter a valid number!\n";
			break;
		}
		system("pause");
		std::cout << std::endl;
	}
	while (1);
	system("pause");

	return  0;
}