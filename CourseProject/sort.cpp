#include "sort.h"

int SortIndex(std::queue<int> &q, int sortedIndex, bool order)
{
	int index = -1;
	int value = INT_MAX;
	if (order == DESC) value = INT_MIN;
	int qSize = q.size();

	for (int i = 0; i < qSize; i++)
	{
		int currentValue = q.front();
		q.pop();

		// When true sort in ascending order else sort in descending
		if (order == DESC)
		{
			if (currentValue >= value && i <= sortedIndex)
			{
				index = i;
				value = currentValue;
			}
		}
		else
		{
			if (currentValue <= value && i <= sortedIndex)
			{
				index = i;
				value = currentValue;
			}
		}

		q.push(currentValue);
	}

	return index;
}

void InsertToRear(std::queue<int> &q, int index)
{
	int value;
	int queueSize = q.size();

	for (int i = 0; i < queueSize; i++)
	{
		int currentValue = q.front();
		q.pop();

		if (i != index)
		{
			q.push(currentValue);
		}
		else
		{
			value = currentValue;
		}
	}

	q.push(value);
}

void SortQueue(std::queue<int> &q, bool order)
{
	for (int i = 1; i <= q.size(); i++)
	{
		int minIndex = SortIndex(q, q.size() - i, order);
		InsertToRear(q, minIndex);
	}
}

void QueuesToStack(std::queue<int> &firstQ, std::queue<int> &secondQ, std::stack<int> &stack)
{
	std::queue<int> tempQ1 = firstQ;
	std::queue<int> tempQ2 = secondQ;

	SortQueue(tempQ1, DESC);
	SortQueue(tempQ2, DESC);

	// TODO: Not working with all
	while (tempQ1.empty() == false && tempQ2.empty() == false)
	{
		if (tempQ1.front() >= tempQ2.front())
		{
			stack.push(tempQ1.front());
			tempQ1.pop();
		}
		else
		{
			stack.push(tempQ2.front());
			tempQ2.pop();
		}
	}
	while (tempQ1.empty() == false)
	{
		stack.push(tempQ1.front());
		tempQ1.pop();
	}
	while (tempQ2.empty() == false)
	{
		stack.push(tempQ2.front());
		tempQ2.pop();
	}
}

void SortBothQueues(std::queue<int> &firstQ, std::queue<int> &secondQ, bool order)
{
	SortQueue(firstQ, order);
	SortQueue(secondQ, order);
}