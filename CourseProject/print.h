#ifndef PRINT_H_
#define PRINT_H_

#include <queue>
#include <stack>

void PrintBothQueues(std::queue<int> &firstQ, std::queue<int> &secondQ);
void PrintQueue(std::queue<int> &q);
void PrintSizeOfQueues(std::queue<int> &firstQ, std::queue<int> &secondQ);
void PrintStackFromFile(const char *fileName);
void PrintNumbersFromFile(const char* fileName);
void PrintStack(std::stack<int> &stack);

#endif /* PRINT_H_ */