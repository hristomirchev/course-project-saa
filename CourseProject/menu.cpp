#include <iostream>
#include "files_functionality.h"
#include "sort.h"
#include "print.h"
#include "defs.h"

int menu()
{
	int selected;
	std::cout << "----------------------------------------";
	std::cout << "\n1. Generate random numbers in file and fill queues.";
	std::cout << "\n2. Read and print random generated numbers from file.";
	std::cout << "\n3. Sort queues.";
	std::cout << "\n4. Print queues.";
	std::cout << "\n5. Transform queues to a stack.";
	std::cout << "\n6. Print stack.";
	std::cout << "\n7. Output the stack to a file.";
	std::cout << "\n8. Read and print stack from file.";
	std::cout << "\n0. Exit";
	std::cout << "\n\nEnter a selection: ";

	std::cin >> selected;
	return selected;
}