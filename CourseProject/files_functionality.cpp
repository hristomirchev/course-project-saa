#include <iostream>
#include <fstream>
#include <ctime>
#include "files_functionality.h"

void GenerateRandomNumbersInFile(const char *fileName)
{
	int numbers;
	std::cout << "Info: The number is multiplied by two because of the two queues\n\n";
	std::cout << "How many numbers do you want to generate?\n";
	std::cout << "Enter an integer: ";
	std::cin >> numbers;

	// Open file as output stream
	std::ofstream file(fileName);

	// Fill the file with random numbers.
	srand(time(0));
	for (int i = 0; i < numbers; i++)
	{
		file << rand() % 150 << " " << rand() % 150 << std::endl;
	}

	// Close the file when done
	file.close();
}

void FillQueuesFromFile(std::queue<int> &firstQ, std::queue<int> &secondQ, const char* fileName)
{
	std::ifstream file(fileName);

	int a, b;

	while (file >> a >> b)
	{
		firstQ.push(a);
		secondQ.push(b);
	}

	file.close();
}

void OutputStackToFile(std::stack<int> &stack, const char* fileName)
{
	std::stack<int> tempStack = stack;
	std::ofstream file(fileName);

	//if (file)
	//{
	while (!tempStack.empty())
	{
		file << tempStack.top() << std::endl;
		tempStack.pop();
	}
	//}
	//else
	//{
	//	std::cout << "File not created" << std::endl;
	//}

	file.close();
}