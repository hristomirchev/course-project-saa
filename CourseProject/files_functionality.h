#ifndef FILES_FUNCTIONALITY_H_
#define FILES_FUNCTIONALITY_H_

#include <queue>
#include <stack>

void GenerateRandomNumbersInFile(const char *fileName);
void FillQueuesFromFile(std::queue<int> &firstQ, std::queue<int> &secondQ, const char* fileName);
void OutputStackToFile(std::stack<int> &stack, const char* fileName);

#endif /* FILES_FUNCTIONALITY_H_ */